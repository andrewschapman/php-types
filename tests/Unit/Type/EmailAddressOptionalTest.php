<?php

namespace Testing\PhpTypes\Unit\Type;

use PhpTypes\Exception\ConstraintException;
use PhpTypes\Type\EmailAddressOptional;
use PHPUnit\Framework\TestCase;

class EmailAddressOptionalTest extends TestCase
{
    /**
     * @param string $emailAddressString
     * @dataProvider validValueProvider
     * @testdox EmailAddressOptional will instantiate with a well-formed email address
     */
    public function testEmailAddressOptionalWillAcceptValidValues(string $emailAddressString): void
    {
        $emailAddress = new EmailAddressOptional($emailAddressString);
        $this->assertEquals($emailAddressString, (string)$emailAddress);
    }

    /**
     * @param string $emailAddress
     * @dataProvider invalidValueProvider
     * @testdox EmailAddressOptional will not instantiate with a badly formed email address
     */
    public function testEmailAddressOptionalWillNotAcceptInvalidValues(string $invalidEmailAddressString): void
    {
        $this->expectException(ConstraintException::class);
        new EmailAddressOptional($invalidEmailAddressString);
    }

    public function validValueProvider(): array
    {
        return [
            ['a@a.com'],
            ['b@b.net'],
            ['c@c.io'],
            ['testyMcTesterson@SomeReallyLongDomain.org.uk'],
            [''],   // EmailAddressOptional will allow empty values
        ];
    }

    public function invalidValueProvider(): array
    {
        return [
            ['a@a'],
            ['@b.net'],
            ['fish'],
            ['b.com@a'],
        ];
    }
}
