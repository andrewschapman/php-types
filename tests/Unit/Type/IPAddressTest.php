<?php

namespace Testing\PhpTypes\Unit\Type;

use PhpTypes\Exception\ConstraintException;
use PhpTypes\Type\IPAddress;
use PHPUnit\Framework\TestCase;

class IPAddressTest extends TestCase
{
    /**
     * @param string $ipAddressString
     * @dataProvider validValueProvider
     * @testdox IPAddress will instantiate with a well formed ip address
     */
    public function testIPAddressWillAcceptValidValues(string $ipAddressString): void
    {
        $emailAddress = new IPAddress($ipAddressString);
        $this->assertEquals($ipAddressString, (string)$emailAddress);
    }

    /**
     * @param string $invalidIPAddressString
     * @dataProvider invalidValueProvider
     * @testdox IPAddress will not instantiate with a badly formed ipAddress
     */
    public function testEmailAddressWillNotAcceptInvalidValues(string $invalidIPAddressString): void
    {
        $this->expectException(ConstraintException::class);
        new IPAddress($invalidIPAddressString);
    }

    public function validValueProvider(): array
    {
        return [
            ['0.0.0.0'],
            ['127.0.0.1'],
            ['192.168.1.1'],
            ['255.255.255.254'],
        ];
    }

    public function invalidValueProvider(): array
    {
        return [
            [''],
            ['127'],
            ['127.0'],
            ['127.0.0'],
            ['.0.0.1'],
            ['127.0.0.'],
            ['aaa.bb.cc.dd'],
        ];
    }
}
