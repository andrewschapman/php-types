<?php

namespace Testing\PhpTypes\Unit\Type;

use PhpTypes\Type\AbstractEnum;
use PHPUnit\Framework\TestCase;

class AbstractEnumTest extends TestCase
{
    /**
     * SCENARIO: Given I instantiate an AbstractEnum with a valid value
     * I expect that the enum is correctly instantiated
     * AND that the value of the enum is the same value I provided to it.
     * @dataProvider validValueProvider
     * @param string $value
     */
    public function testAbstractEnumWillInstantitateWithValidValue($value): void
    {
        $enum = $this->getAbstractEnum($value);
        $this->assertEquals($value, (string)$enum);
    }

    /**
     * SCENARIO: Given I instantiate an AbstractEnum with a valid value
     * I expect that the enum is correctly instantiated
     * AND that the value of the enum is the same value I provided to it.
     * @dataProvider invalidValueProvider
     * @param string $value
     */
    public function testAbstractEnumWillThrowExceptionWithInvalidValue($value): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $enum = $this->getAbstractEnum($value);
    }

    private function getAbstractEnum($value): AbstractEnum
    {
        return new Class($value) extends AbstractEnum {
            public const BLUE = 'blue';
            public const GREEN = 'green';
            public const RED = 'red';
            public const OFF_WHITE = 'off_white';
        };
    }

    public function validValueProvider(): array
    {
        return [
            ['blue'],
            ['green'],
            ['red'],
            ['off_white']
        ];
    }

    public function invalidValueProvider(): array
    {
        return [
            ['tangerine'],
            ['pink'],
            [''],
            [1]
        ];
    }
}
