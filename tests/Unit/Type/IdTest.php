<?php

namespace Testing\PhpTypes\Unit\Type;

use PhpTypes\Exception\ConstraintException;
use PhpTypes\Type\Id;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Lazy\LazyUuidFromString;

class IdTest extends TestCase
{
    public function testIdWillCreateNewValueWhenBlank(): void
    {
        $id = $this->getId('', true);
        $this->assertNotEmpty($id);
        $this->assertInstanceOf(LazyUuidFromString::class, $id->getUuid());
    }

    public function testIdWillPopulateFromSpecifiedValue(): void
    {
        $id = $this->getId('bca90489-4127-46e0-ab8c-13edb02ad083', false);
        $this->assertEquals('bca90489-4127-46e0-ab8c-13edb02ad083', $id->getUuid()->toString());
    }

    public function testIdWillThrowExceptionIfEmptyAndNotAllowedToBe(): void
    {
        $this->expectException(ConstraintException::class);
        $this->getId('', false);
    }

    private function getId(string $uuid, bool $generateNewIdIfEmpty): Id
    {
        return new Class($uuid, $generateNewIdIfEmpty) extends Id
        {
            public function __construct(string $uuid = '', bool $generateNewIdIfEmpty = false)
            {
                parent::__construct($uuid, $generateNewIdIfEmpty);
            }
        };
    }
}
