<?php

namespace Testing\PhpTypes\Unit\Type;

use PhpTypes\Collection\Dictionary;
use PhpTypes\Type\DictionaryKey;
use PhpTypes\Type\DictionaryKeyMode;
use PHPUnit\Framework\TestCase;

class DictionaryKeyTest extends TestCase
{
    /**
     * SCENARIO: Given I create a DictionaryKey object with one of the available keyModes: LOWER, UPPER, CAMEL_CASE, NATURAL
     * THEN I expect that the key value passed in will be transformed correctly according to the mode.
     * @param string $initialKey
     * @param string $transformedKey
     * @param string $keyMode
     * @throws \ReflectionException
     * @dataProvider keyModeTestProvider
     */
    public function testDictionaryKeyWillTransformCorrectly(string $initialKey, string $transformedKey, string $keyMode): void
    {
        $dictionary = new Dictionary(true, new DictionaryKeyMode($keyMode));

        $dictionaryKey = $dictionary->createKey($initialKey);

        $this->assertEquals($transformedKey, (string)$dictionaryKey);
    }

    public function keyModeTestProvider(): array
    {
        return [
            // LOWER CASE CONVERSIONS
            ['SOMEKEY', 'somekey', DictionaryKeyMode::LOWER],
            ['SOMEkey', 'somekey', DictionaryKeyMode::LOWER],
            ['SOME-key', 'some-key', DictionaryKeyMode::LOWER],
            ['SOME_key', 'some_key', DictionaryKeyMode::LOWER],
            ['_SOME_key', '_some_key', DictionaryKeyMode::LOWER],
            [':SOME_key', ':some_key', DictionaryKeyMode::LOWER],

            // UPPER CASE CONVERSIONS
            ['somekey', 'SOMEKEY', DictionaryKeyMode::UPPER],
            ['someKey', 'SOMEKEY', DictionaryKeyMode::UPPER],
            ['SoMeKey', 'SOMEKEY', DictionaryKeyMode::UPPER],
            ['SOME-key', 'SOME-KEY', DictionaryKeyMode::UPPER],
            ['SOME_key', 'SOME_KEY', DictionaryKeyMode::UPPER],

            // CAMEL CASE CONVERSIONS
            ['some_key', 'someKey', DictionaryKeyMode::CAMEL_CASE],
            ['some key', 'someKey', DictionaryKeyMode::CAMEL_CASE],
            ['somekey', 'somekey', DictionaryKeyMode::CAMEL_CASE],
            ['SomeKey', 'someKey', DictionaryKeyMode::CAMEL_CASE],

            // NATURAL CONVERSION
            ['some_key', 'some_key', DictionaryKeyMode::NATURAL],
            ['somekey', 'somekey', DictionaryKeyMode::NATURAL],
            ['Somekey', 'Somekey', DictionaryKeyMode::NATURAL],
            ['SOMEKEY', 'SOMEKEY', DictionaryKeyMode::NATURAL],
        ];
    }
}
