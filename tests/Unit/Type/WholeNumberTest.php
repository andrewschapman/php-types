<?php

namespace Testing\PhpTypes\Unit\Type;

use PhpTypes\Exception\ConstraintException;
use PhpTypes\Type\WholeNumber;
use PHPUnit\Framework\TestCase;

class WholeNumberTest extends TestCase
{
    /**
     * SCENARIO: If I create a WholeNumber with a valid value
     * THEN I expect that the class will be created correctly
     * AND that the value returned will be valid.
     * @dataProvider correctValueProvider
     */
    public function testWholeNumberAllowsValidValues(int $value): void
    {
        $wholeNumber = new WholeNumber($value);
        $this->assertEquals($value, $wholeNumber->getValue());
    }

    /**
     * SCENARIO: If I create a WholeNumber with an invalid value
     * THEN I expect that a ConstraintException will be thrown.
     * @dataProvider incorrectValueProvider
     */
    public function testWholeNumberAllowsInvalidValues(int $value): void
    {
        $this->expectException(ConstraintException::class);
        $wholeNumber = new WholeNumber($value);
    }

    public function correctValueProvider(): array
    {
        return [
            [0],
            [1],
            [100],
            [1000]
        ];
    }

    public function incorrectValueProvider(): array
    {
        return [
            [-1],
            [-100],
            [-1000]
        ];
    }
}


