<?php

namespace Testing\PhpTypes\Unit\Type;

use PhpTypes\Type\IsoDate;
use PhpTypes\Exception\ConstraintException;
use PHPUnit\Framework\TestCase;

class IsoDateTest extends TestCase
{
    public function testCanBeCreatedFromValidDateString(): void
    {
        $isoDate = new IsoDate('2023-12-25');
        $this->assertInstanceOf(IsoDate::class, $isoDate);
        $this->assertEquals('2023-12-25', $isoDate->getValue());
    }

    public function testCanBeCreatedFromTimestamp(): void
    {
        $timestamp = strtotime('2023-12-25');
        $isoDate = new IsoDate($timestamp);
        $this->assertInstanceOf(IsoDate::class, $isoDate);
        $this->assertEquals('2023-12-25', $isoDate->getValue());
    }

    public function testInvalidDateStringThrowsException(): void
    {
        $this->expectException(ConstraintException::class);
        new IsoDate('invalid-date');
    }

    public function testToStringReturnsCorrectFormat(): void
    {
        $isoDate = new IsoDate('2023-12-25');
        $this->assertEquals('2023-12-25', $isoDate->toString());
    }

    public function testToTimestampReturnsCorrectTimestamp(): void
    {
        $isoDate = new IsoDate('2023-12-25');
        $expectedTimestamp = strtotime('2023-12-25 00:00:00');
        $this->assertEquals($expectedTimestamp, $isoDate->toTimestamp());
    }
}
