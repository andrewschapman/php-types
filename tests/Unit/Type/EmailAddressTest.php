<?php

namespace Testing\PhpTypes\Unit\Type;

use PhpTypes\Exception\ConstraintException;
use PhpTypes\Type\EmailAddress;
use PHPUnit\Framework\TestCase;

class EmailAddressTest extends TestCase
{
    /**
     * @param string $emailAddressString
     * @dataProvider validValueProvider
     * @testdox Email address will instantiate with a well formed email address
     */
    public function testEmailAddressWillAcceptValidValues(string $emailAddressString): void
    {
        $emailAddress = new EmailAddress($emailAddressString);
        $this->assertEquals($emailAddressString, (string)$emailAddress);
    }

    /**
     * @param string $emailAddress
     * @dataProvider invalidValueProvider
     * @testdox Email address will not instantiate with a badly formed email address
     */
    public function testEmailAddressWillNotAcceptInvalidValues(string $invalidEmailAddressString): void
    {
        $this->expectException(ConstraintException::class);
        new EmailAddress($invalidEmailAddressString);
    }

    public function validValueProvider(): array
    {
        return [
            ['a@a.com'],
            ['b@b.net'],
            ['c@c.io'],
            ['testyMcTesterson@SomeReallyLongDomain.org.uk'],
        ];
    }

    public function invalidValueProvider(): array
    {
        return [
            ['a@a'],
            ['@b.net'],
            ['fish'],
            ['b.com@a'],
        ];
    }
}
