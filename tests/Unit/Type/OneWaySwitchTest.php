<?php

namespace Testing\PhpTypes\Unit\Type;

use PhpTypes\Type\OneWaySwitch;
use PHPUnit\Framework\TestCase;

class OneWaySwitchTest extends TestCase
{
    /**
     * @testdox OneWaySwitch will trigger and give correct results for isDone and isNotDone
     */
    public function testOneWaySwitchWillTrigger(): void
    {
        $switch = new OneWaySwitch();
        $this->assertFalse($switch->isDone());
        $this->assertTrue($switch->isNotDone());

        // Trigger the switch
        $switch->complete();

        $this->assertTrue($switch->isDone());
        $this->assertFalse($switch->isNotDone());

        // If we trigger the switch again, nothing changes.
        $switch->complete();

        $this->assertTrue($switch->isDone());
        $this->assertFalse($switch->isNotDone());
    }
}
