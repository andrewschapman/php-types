<?php

namespace Testing\PhpTypes\Unit\Type;

use PhpTypes\Exception\ConstraintException;
use PhpTypes\Type\ConstrainedString;
use PHPUnit\Framework\TestCase;

class ContainedStringTest extends TestCase
{
    /**
     * SCENARIO: If I try and instantiate a ConstrainedString that requires a value with no value
     * THEN I expect to see that a ConstraintException is thrown.
     */
    public function testConstrainedStringWillThrowExceptionIfValueTooShort(): void
    {
        $this->expectException(ConstraintException::class);
        $this->getConstrainedString('');
    }

    /**
     * SCENARIO: If I try and instantiate a ConstrainedString that enforces a maximum with a value that is too long
     * THEN I expect to see that a ConstraintException is thrown.
     */
    public function testConstrainedStringWillThrowExceptionIfValueTooLong(): void
    {
        $this->expectException(ConstraintException::class);
        $this->getConstrainedString('I AM LONGER THAN 20 CHARACTERS');
    }

    /**
     * SCENARIO: If I try and instantiate a ConstrainedString with a valid value
     * THEN I expect to see that the class is created correctly
     * AND that the value returned is the same as the value I create it with.
     */
    public function testConstrainedStringWillAllowCorrectValue(): void
    {
        $myString = $this->getConstrainedString('Banana Man');
        $this->assertEquals('Banana Man', $myString->getValue());
    }

	public function testConstrainedStringComparisonsWorkCorrectly(): void
	{
		$myString = $this->getConstrainedString('Banana Man');
		$myString2 = $this->getConstrainedString('Apple Lady');
		$myString3 = $this->getConstrainedString('banana man');

		$this->assertTrue($myString->equals($myString));
		$this->assertTrue($myString->notEquals($myString2));
		$this->assertTrue($myString->notEquals($myString3));
		$this->assertTrue($myString->equalsCaseInsensitive($myString3));
		$this->assertFalse($myString->notEqualsCaseInsensitive($myString3));
	}

    public function testConstrainedStringWillReturnAndLastFirstCharacter(): void
    {
        $myString = $this->getConstrainedString('Banana Man');
        $this->assertEquals('B', $myString->firstChar());
        $this->assertEquals('n', $myString->lastChar());
    }

    private function getConstrainedString(string $value): ConstrainedString
    {
        return new Class($value) extends ConstrainedString {
            public function __construct(string $value)
            {
                parent::__construct($value, 1, 20);
            }
        };
    }
}
