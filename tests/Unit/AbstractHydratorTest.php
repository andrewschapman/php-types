<?php

namespace Testing\PhpTypes\Unit;

use Exception;
use Faker\Factory;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Testing\PhpTypes\Unit\Support\User\Entities\UserHydrator;

class AbstractHydratorTest extends TestCase
{
    /** @testdox The Hydrator class will throw an exception if a field is missing entirely from the data */
    public function testMapperWillThrowExceptionIfFieldMissing(): void
    {
        $this->expectException(Exception::class);

        $data = [
            'firstName' => 'Charlie',
            'lastName' => 'Smith',
            'age' => 95,
            'phone' => '(03) 9876 1234',
            'isHuman' => true,
            'numCats' => 2,
            'price' => 200.50
        ];

        UserHydrator::hydrate($data);
    }

    /** @testdox The Hydrator class will hydrate an object if all fields are present */
    public function testMapperWillHydrateFields(): void
    {
        $data = [
            'id' => '04fc13a6-6ee1-466c-a001-b4b08b95c71e',
            'firstName' => 'Charlie',
            'lastName' => 'Smith',
            'age' => 95,
            'phone' => '(03) 9876 1234',
            'isHuman' => true,
            'numCats' => 2,
            'price' => 200.50
        ];

        $user = UserHydrator::hydrate($data);

        self::assertEquals($data['id'], $user->getId()->toString());
        self::assertEquals($data['firstName'], $user->getFirstName()->toString());
        self::assertEquals($data['lastName'], $user->getLastName()->toString());
        self::assertEquals($data['age'], $user->getAge()->getValue());
        self::assertEquals($data['phone'], $user->getPhone());
        self::assertEquals($data['isHuman'], $user->isHuman());
        self::assertEquals($data['numCats'], 2);
        self::assertEquals($data['price'], 200.50);
    }

    /** @testdox The Hydrator class will hydrate an object if a primitive value is null and null is allowed */
    public function testMapperWillHydrateNullField(): void
    {
        $data = [
            'id' => '04fc13a6-6ee1-466c-a001-b4b08b95c71e',
            'firstName' => 'Charlie',
            'lastName' => 'Smith',
            'age' => 95,
            'phone' => null,
            'isHuman' => false,
            'numCats' => 2,
            'price' => 200.50
        ];

        $user = UserHydrator::hydrate($data);

        self::assertEquals($data['id'], $user->getId()->toString());
        self::assertEquals($data['firstName'], $user->getFirstName()->toString());
        self::assertEquals($data['lastName'], $user->getLastName()->toString());
        self::assertEquals($data['age'], $user->getAge()->getValue());
        self::assertNull($user->getPhone());
        self::assertEquals($data['isHuman'], $user->isHuman());
        self::assertEquals($data['numCats'], 2);
        self::assertEquals($data['price'], 200.50);
    }

    /**
     * @testdox The Hydrator class will hydrate an object if a primitive boolean is passed as a string or int
     * @dataProvider alternativeBooleanValueProvider
     */
    public function testMapperWillHydrateBooleanExpressedAsAlternativeFormat($isHuman, bool $expected): void
    {
        $data = [
            'id' => '04fc13a6-6ee1-466c-a001-b4b08b95c71e',
            'firstName' => 'Charlie',
            'lastName' => 'Smith',
            'age' => 95,
            'phone' => null,
            'isHuman' => $isHuman,
            'numCats' => 2,
            'price' => 200.50
        ];

        $user = UserHydrator::hydrate($data);

        self::assertEquals($expected, $user->isHuman());
    }

    /** @testdox The Hydrator class will hydrate an array of arrays of data into a list object */
    public function testMapperWillHydrateList(): void
    {
        $numRows = 1000;

        $data = $this->generateUserData($numRows);

        //$start = microtime(true);
        $userList = UserHydrator::hydrateList($data);
        //$finish = microtime(true);

        self::assertCount($numRows, $userList);

        //$diff = $finish - $start;

        //print "Hydration time for $numRows rows: $diff\n";
    }

    /**
     * Generates fake user data for performance testing.
     * @param int $numRecords
     * @return array
     * @throws Exception
     */
    private function generateUserData(int $numRecords): array
    {
        $faker = Factory::create();
        mt_srand();

        $rows = [];

        for ($counter = 0; $counter < $numRecords; $counter++) {
            $row = [];
            $row['id'] = Uuid::uuid4()->toString();
            $row['firstName'] = $faker->name();
            $row['lastName'] = $faker->name();
            $row['age'] = random_int(0, 99);
            $row['phone'] = $faker->phoneNumber();
            $row['isHuman'] = random_int(0, 1) ? true : false;
            $row['numCats'] = random_int(0, 10);
            $row['price'] = $faker->randomFloat(2);

            $rows[] = $row;
        }

        return $rows;
    }

    /**
     * Provides test cases to ensure when booleans are passed through in alternative formats that
     * they are correctly evaluated.
     * @return array[]
     */
    public function alternativeBooleanValueProvider(): array
    {
        return [
            ['true', true],
            ['t', true],
            [1, true],
            ['false', false],
            ['f', false],
            [0, false],
            [2, false],
            [-1, false],
        ];
    }
}
