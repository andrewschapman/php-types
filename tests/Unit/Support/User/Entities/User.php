<?php

namespace Testing\PhpTypes\Unit\Support\User\Entities;

use Ramsey\Uuid\UuidInterface;
use Testing\PhpTypes\Unit\Support\User\Types\Age;
use Testing\PhpTypes\Unit\Support\User\Types\FirstName;
use Testing\PhpTypes\Unit\Support\User\Types\LastName;

class User
{
    /** @var UuidInterface */
    private $id;

    /** @var FirstName */
    private $firstName;

    /** @var LastName */
    private $lastName;

    /** @var Age */
    private $age;

    /** @var string|null */
    private $phone;

    /** @var bool */
    private $isHuman;

    /** @var int */
    private $numCats;

    /** @var float */
    private $price;

    public function __construct(
        UuidInterface $id,
        FirstName $firstName,
        LastName $lastName,
        Age $age,
        ?string $phone,
        bool $isHuman,
        int $numCats,
        float $price
    ) {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->age = $age;
        $this->phone = $phone;
        $this->isHuman = $isHuman;
        $this->numCats = $numCats;
        $this->price = $price;
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return FirstName
     */
    public function getFirstName(): FirstName
    {
        return $this->firstName;
    }

    /**
     * @return LastName
     */
    public function getLastName(): LastName
    {
        return $this->lastName;
    }

    /**
     * @return Age
     */
    public function getAge(): Age
    {
        return $this->age;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @return bool
     */
    public function isHuman(): bool
    {
        return $this->isHuman;
    }

    /**
     * @return int
     */
    public function getNumCats(): int
    {
        return $this->numCats;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }
}
