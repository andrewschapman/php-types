<?php

namespace Testing\PhpTypes\Unit\Support\User\Entities;

use PhpTypes\AbstractHydrator;
use PhpTypes\ValueObjects\FieldDefinition;
use Ramsey\Uuid\Uuid;
use Testing\PhpTypes\Unit\Support\User\Lists\UserList;
use Testing\PhpTypes\Unit\Support\User\Types\Age;
use Testing\PhpTypes\Unit\Support\User\Types\FirstName;
use Testing\PhpTypes\Unit\Support\User\Types\LastName;

class UserHydrator extends AbstractHydrator
{
    protected static function getMap(): array
    {
        return [
            new FieldDefinition('id', Uuid::class),
            new FieldDefinition('firstName', FirstName::class),
            new FieldDefinition('lastName', LastName::class),
            new FieldDefinition('age', Age::class),
            new FieldDefinition('phone', AbstractHydrator::FIELD_TYPE_STRING),
            new FieldDefinition('isHuman', AbstractHydrator::FIELD_TYPE_BOOLEAN),
            new FieldDefinition('numCats', AbstractHydrator::FIELD_TYPE_INT),
            new FieldDefinition('price', AbstractHydrator::FIELD_TYPE_FLOAT)
        ];
    }

    public static function hydrate(array $data): User
    {
        $fields = parent::hydrateFields($data);

        return new User(...$fields);
    }

    public static function hydrateList(array $data): UserList
    {
        $userList = new UserList();

        foreach (parent::hydrateFieldRange($data) as $fields) {
            $userList->add(new User(...$fields));
        }

        return $userList;
    }
}
