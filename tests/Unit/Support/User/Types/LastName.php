<?php

namespace Testing\PhpTypes\Unit\Support\User\Types;

use PhpTypes\Type\ConstrainedString;

class LastName extends ConstrainedString
{
    /**
     * FirstName constructor.
     */
    public function __construct(string $name)
    {
        parent::__construct($name, 2, 50);
    }
}
