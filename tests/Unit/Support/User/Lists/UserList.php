<?php


namespace Testing\PhpTypes\Unit\Support\User\Lists;

use PhpTypes\Collection\AbstractList;
use Testing\PhpTypes\Unit\Support\User\Entities\User;

class UserList extends AbstractList
{
    public function add(User $item): void
    {
        $this->values[] = $item;
    }

    public function current(): User
    {
        return $this->offsetGet($this->iteratorPointer);
    }

    public function offsetGet($offset): User
    {
        return $this->values[$offset];
    }
}
