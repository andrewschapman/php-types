<?php

namespace PhpTypes\ValueObjects;

final class FieldDefinition
{
    /** @var string */
    private $dbColumn;

    /** @var string */
    private $classFieldType;

    public function __construct(string $dbColumn, string $classFieldType)
    {
        $this->dbColumn = $dbColumn;
        $this->classFieldType = $classFieldType;
    }

    /**
     * @return string
     */
    public function getDbColumn(): string
    {
        return $this->dbColumn;
    }


    /**
     * @return string
     */
    public function getClassFieldType(): string
    {
        return $this->classFieldType;
    }
}
