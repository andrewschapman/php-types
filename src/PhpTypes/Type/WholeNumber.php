<?php

namespace PhpTypes\Type;

use PhpTypes\Exception\ConstraintException;

/**
 * Ensures that numbers are "Whole" numbers, e.g. integers that are >= 0
 */
class WholeNumber
{
    /** @var int */
    protected $value;

    public function __construct(int $value)
    {
        if ($value < 0) {
            throw new ConstraintException('WholeNumber value must be >= 0');
        }

        $this->value = $value;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function toString(): string
    {
        return strval($this->value);
    }

    public function increment(): void
    {
        $this->value++;
    }
}
