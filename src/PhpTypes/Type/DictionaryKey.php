<?php

namespace PhpTypes\Type;

use PhpTypes\Collection\Dictionary;
use PhpTypes\Helper\StringHelper;

class DictionaryKey extends ConstrainedString
{
    public function __construct(string $keyName, Dictionary $dictionary)
    {
        // Transform keys into upper case, lower case
        $keyName = $this->transformKey($keyName, $dictionary->getKeyMode());

        parent::__construct($keyName, 1, 255);
    }

    private function transformKey(string $keyName, ?DictionaryKeyMode $keyMode): string
    {
        if (is_null($keyMode)) {
            return $keyName;
        }

        switch ((string)$keyMode) {
            case DictionaryKeyMode::CAMEL_CASE:
                return StringHelper::camelCase($keyName);
                break;

            case DictionaryKeyMode::LOWER:
                return strtolower($keyName);
                break;

            case DictionaryKeyMode::UPPER:
                return strtoupper($keyName);
                break;

            default:
                return $keyName;
        }
    }
}
