<?php

namespace PhpTypes\Type;

use PhpTypes\Exception\ConstraintException;

/**
 * Must be extended to be used.
 * Ensures string values are within min/max length criteria.
 */
abstract class ConstrainedString
{
    /** @var string */
    private $value;

    public function __construct(string $value, int $minLength = 0, int $maxLength = 0)
    {
        $stringLen = strlen($value);

        if (($minLength > 0) && ($stringLen < $minLength)) {
            throw new ConstraintException(
                sprintf('Invalid %s, value must be at least %d characters', static::class, $minLength)
            );
        }

        if (($maxLength > 0) && ($stringLen > $maxLength)) {
            throw new ConstraintException(
                sprintf('Invalid %s, value must be no longer than %d characters', static::class, $maxLength)
            );
        }

        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function toString(): string
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return $this->value;
    }

    public function equals(ConstrainedString $comparisonString): bool
    {
        return $comparisonString->toString() === $this->getValue();
    }

    public function equalsCaseInsensitive(ConstrainedString $comparisonString): bool
    {
        return strtolower($comparisonString->toString()) === strtolower($this->getValue());
    }

    public function notEquals(ConstrainedString $comparisonString): bool
    {
        return $this->equals($comparisonString) == false;
    }

    public function notEqualsCaseInsensitive(ConstrainedString $comparisonString): bool
    {
        return $this->equalsCaseInsensitive($comparisonString) == false;
    }

    /**
     * Returns the first character of the string
     * @return string
     */
    public function firstChar(): string
    {
        if (empty($this->value)) {
            return '';
        }

        return $this->value[0];
    }

    /**
     * Returns the last character of the string
     * @return string
     */
    public function lastChar(): string
    {
        if (empty($this->value)) {
            return '';
        }

        return $this->value[strlen($this->value) - 1];
    }
}
