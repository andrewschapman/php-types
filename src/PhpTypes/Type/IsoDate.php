<?php

declare(strict_types=1);

namespace PhpTypes\Type;

use PhpTypes\Exception\ConstraintException;

class IsoDate
{
    private string $value;

    public function __construct(string|int $date)
    {
        if (is_int($date)) {
            $this->value = $this->convertFromTimestamp($date);
        } elseif (is_string($date)) {
            $this->validateIsoFormat($date);
            $this->value = $date;
        } else {
            throw new ConstraintException('IsoDate must be initialized with a string or an integer.');
        }
    }

    private function convertFromTimestamp(int $timestamp): string
    {
        return date('Y-m-d', $timestamp);
    }

    private function validateIsoFormat(string $date): void
    {
        $regex = '/^\d{4}-\d{2}-\d{2}$/';
        if (!preg_match($regex, $date)) {
            throw new ConstraintException('Invalid ISO date format.');
        }
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function toString(): string
    {
        return $this->getValue();
    }

    public function toTimestamp(): int
    {
        return strtotime($this->value . ' 00:00:00');
    }
}
