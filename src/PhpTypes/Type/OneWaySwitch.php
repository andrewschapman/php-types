<?php

namespace PhpTypes\Type;

/**
 * Use this class if you need to set a boolean value
 * exactly once and then prevent that value from changing.
 * E.g. if you're in a loop and you want to do something just once.
 */
class OneWaySwitch
{
    private $isCompleted = false;

    public function complete(): void
    {
        if ($this->isCompleted) {
            return;
        }

        $this->isCompleted = true;
    }

    public function isDone(): bool
    {
        return $this->isCompleted;
    }

    public function isNotDone(): bool
    {
        return !$this->isDone();
    }
}
