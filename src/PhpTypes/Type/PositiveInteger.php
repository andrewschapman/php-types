<?php

namespace PhpTypes\Type;

use PhpTypes\Exception\ConstraintException;

/**
 * Ensures that numbers are positive integers, e.g. > 0/
 */
class PositiveInteger
{
    /** @var int */
    protected $value;

    public function __construct(int $value)
    {
        if ($value < 1) {
            throw new ConstraintException('PositiveInteger value must be > 0');
        }

        $this->value = $value;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function toString(): string
    {
        return strval($this->value);
    }
}
