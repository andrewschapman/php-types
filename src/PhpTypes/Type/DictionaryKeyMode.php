<?php

namespace PhpTypes\Type;

class DictionaryKeyMode extends AbstractEnum
{
    public const CAMEL_CASE = 'camel_case';
    public const LOWER = 'lower';
    public const NATURAL = 'natural';
    public const UPPER = 'upper';
}
