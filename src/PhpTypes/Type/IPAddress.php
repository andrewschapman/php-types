<?php

namespace PhpTypes\Type;

use PhpTypes\Exception\ConstraintException;

class IPAddress extends ConstrainedString
{
    public function __construct(string $ipAddress, int $minLength = 0, int $maxLength = 0)
    {
        parent::__construct($ipAddress, 7, 0);

        if (!filter_var($ipAddress, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
            throw new ConstraintException("Invalid ip address: $ipAddress");
        }
    }
}
