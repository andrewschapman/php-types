<?php

namespace PhpTypes\Type;

use PhpTypes\Exception\ConstraintException;

class EmailAddress extends ConstrainedString
{
    public function __construct(string $emailAddress)
    {
        // Remove any white space
        $emailAddress = trim($emailAddress);

        parent::__construct($emailAddress, 6, 0);

        if (!filter_var($emailAddress, FILTER_VALIDATE_EMAIL)) {
            throw new ConstraintException("Invalid email address: $emailAddress");
        }
    }
}
