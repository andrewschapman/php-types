<?php

namespace PhpTypes\Type;

use PhpTypes\Exception\ConstraintException;
use Exception;

/**
 * Class Timestamp
 * @package App\Core\Type
 *
 * Represents a unix timestamp.
 */
class Timestamp
{
    /** @var int */
    private $timestamp;

    public function __construct($init = null)
    {
        if ((is_null($init)) || (empty($init))) {
            $this->timestamp = time();
        } else if ($init instanceof Timestamp) {
            $this->timestamp = $init->getTimestamp();
        } else if (is_numeric($init)) {
            $this->timestamp = $init;
        } else {
            $this->timestamp = strtotime($init);

            if ($this->timestamp === false) {
                throw new ConstraintException("Invalid timestamp value value '$init'");
            }
        }
    }

    public function getTimestamp(): int
    {
        return $this->timestamp;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function __toString()
    {
        $result = date('Y-m-d H:i:s', $this->timestamp);

        if (!$result) {
            throw new \Exception('Timestamp could not be converted to string - invalid timestamp');
        }

        return $result;
    }
}
