<?php

namespace PhpTypes\Type;

use InvalidArgumentException;
use ReflectionException;
use ReflectionClass;

/**
 * Uses reflection to make sure values are from the list of class constants
 */
abstract class AbstractEnum
{
    /** @var mixed */
    protected $value;

    /**
     * @throws ReflectionException
     */
    public function __construct($value)
    {
        $this->check($value);
        $this->value = $value;
    }

    public function __toString()
    {
        return strval($this->value);
    }

    public function is($value): bool
    {
        return ($this->value === (string)$value);
    }

    public function in(array $values): bool
    {
        return in_array($this->value, $values);
    }

    public function notIn(array $values): bool
    {
        return !$this->in($values);
    }

    public function isNot($value): bool
    {
        return !$this->is($value);
    }

    /**
     * @throws ReflectionException
     */
    public static function valueList(): array
    {
        $refl = new ReflectionClass(static::class);
        return $refl->getConstants();
    }

    /**
     * @param $value
     * @throws ReflectionException
     */
    private function check($value): void
    {
        $refl = new ReflectionClass($this);
        $constants = $refl->getConstants();

        if (!in_array($value, $constants, true)) {
            $className = static::class;
            $allowedValues = implode(', ', $constants);
            throw new InvalidArgumentException(
                "{$className} Value must be one of the pre-defined constants. {$value} is not in list " .
                "of allowed values ({$allowedValues})"
            );
        }
    }
}
