<?php

namespace PhpTypes\Type;

use PhpTypes\Exception\ConstraintException;
use Ramsey\Uuid\Uuid;

/**
 * Creates and stores a UUID, useful for database identifier.
 */
abstract class Id
{
    private $uuid;

    public function __construct(string $uuid = '', $generateNewIdIfEmpty = false)
    {
        if (!empty($uuid)) {
            $this->uuid = Uuid::fromString($uuid);
        } else {
            if (!$generateNewIdIfEmpty) {
                throw new ConstraintException(static::class . ' must have a valid value');
            }

            $this->uuid = Uuid::uuid4();
        }
    }

    public function getUuid(): \Ramsey\Uuid\UuidInterface
    {
        return $this->uuid;
    }

    public function __toString()
    {
        return $this->uuid->toString();
    }

	public function equals(Id $anotherId): bool
	{
		return $this->getUuid()->equals($anotherId->getUuid());
	}
}
