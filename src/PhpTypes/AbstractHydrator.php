<?php

namespace PhpTypes;

use PhpTypes\Exception\ConstraintException;
use PhpTypes\ValueObjects\FieldDefinition;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Generator;

/**
 * Hydrates objects from an array of data given a field map definition.
 * Objects can be hydrated with primitive values (string, int, float, bool)
 * They can also be hydrated with custom types.
 */
abstract class AbstractHydrator
{
    public const FIELD_TYPE_BOOLEAN = 'bool';
    public const FIELD_TYPE_FLOAT = 'float';
    public const FIELD_TYPE_INT = 'int';
    public const FIELD_TYPE_STRING = 'string';

    /**
     * When you extend this class, override this method.
     * Flesh this out with key-value pairs of
     * field_name => fieldType,  e.g.
     * 'id' => 'string',
     * 'firstName' => FirstName::class
     * @return array
     */
    protected static function getMap(): array#
    {
        return [];
    }

    /**
     * Loops through an array of data and creates hydrated objects where possible.  Primitive values will
     * be returned 'as is'.  Custom Type classes will be instantiated.
     * @param array $data
     * @return array
     */
    protected static function hydrateFields(array $data): array
    {
        $map = static::getMap();

        $fields = [];

        /** @var FieldDefinition $fieldDefinition */
        foreach ($map as $fieldDefinition) {
            $columnName = $fieldDefinition->getDbColumn();

            if (!array_key_exists($columnName, $data)) {
                throw new ConstraintException("AbstractMapper::hydrate - data missing '$columnName' attribute");
            }

            $fieldValue = $data[$columnName];

            if (null === $fieldValue) {
                $fields[] = null;
                continue;
            }

            $classFieldType = $fieldDefinition->getClassFieldType();

            /**
             * Note that is deliberately left in-line and not broken out into a separate function for performance
             * reasons.  If you have 100,000 records for example, there is a 5-10% difference when you don't
             * have the overhead of calling a function.
             */
            switch ($classFieldType) {
                case self::FIELD_TYPE_STRING:
                    $fields[] = $fieldValue;
                    break;

                case self::FIELD_TYPE_INT:
                    $fields[] = (int)$fieldValue;
                    break;

                case self::FIELD_TYPE_FLOAT:
                    $fields[] = (float)$fieldValue;
                    break;

                case self::FIELD_TYPE_BOOLEAN:
                    if (is_bool($fieldValue)) {
                        $fields[] = $fieldValue;
                        break;
                    }

                    if (is_int($fieldValue)) {
                        $fields[] = $fieldValue === 1;
                        break;
                    }

                    if (($fieldValue === 'true') || ($fieldValue === 't')) {
                        $fields[] = true;
                        break;
                    }

                    $fields[] = false;
                    break;

                case Uuid::class:
                case UuidInterface::class:
                    $fields[] = Uuid::fromString($fieldValue);
                    break;

                default:
                    $fields[] = new $classFieldType($fieldValue);
                    break;
            }
        }

        return $fields;
    }

    /**
     * Excepts an array of arrays where each array is a row of data (e.g. from a database).
     * Each row will be hydrated with custom types and resulting hydrated field array is yielded,
     * so consumers can foreach through the hydrated fields.
     * @param array $data
     * @return Generator
     */
    public static function hydrateFieldRange(array $data)
    {
        foreach ($data as $indexValue) {
            yield self::hydrateFields($indexValue);
        }
    }
}
