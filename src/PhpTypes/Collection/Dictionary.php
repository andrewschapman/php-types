<?php

namespace PhpTypes\Collection;

use PhpTypes\Exception\KeyAlreadyExistsException;
use PhpTypes\Helper\JsonHelper;
use PhpTypes\Type\DictionaryKey;
use PhpTypes\Type\DictionaryKeyMode;
use ReturnTypeWillChange;

/**
 * Implements a simple Dictionary object with set/get/exists method.
 * The object is iterable so you can iterate through the key/value pairs.
 * Note that keys are DictionaryKey objects which allow you to ensure all keys have consistent casing.
 */
class Dictionary implements \Iterator
{
    /** @var array */
    private $values = [];

    /** @var array */
    private $keys = [];

    /** @var int */
    private $currentIndex = 0;

    /** @var bool */
    private $allowOverwriteExistingKeys;

    /** @var DictionaryKeyMode */
    private $keyMode;

    public function __construct(bool $allowOverwriteExistingKeys = true, ?DictionaryKeyMode $keyMode = null)
    {
        $this->allowOverwriteExistingKeys = $allowOverwriteExistingKeys;
        $this->keyMode = $keyMode;

        if (is_null($this->keyMode)) {
            $this->keyMode = new DictionaryKeyMode(DictionaryKeyMode::NATURAL);
        }
    }

    /**
     * Sets a new value for the given dictionary key in the dictionary.
     * If overwriting new values is not allowed and you try to redefine an existing key
     * then a KeyAlreadyExistsException will be thrown.
     * @param DictionaryKey $key
     * @param mixed $value
     * @throws KeyAlreadyExistsException
     */
    public function set(DictionaryKey $key, $value): void
    {
        if (!$this->allowOverwriteExistingKeys && $this->exists($key)) {
            throw new KeyAlreadyExistsException((string)$key);
        }

        $this->values[(string)$key] = $value;
        $this->keys[] = $key;
    }

    /**
     * Returns the value of the dictionary key.  Null if the key doesn't exist.
     * @param DictionaryKey $key
     * @return mixed|null
     */
    public function get(DictionaryKey $key)
    {
        if (!$this->exists($key)) {
            return null;
        }

        return $this->values[(string)$key];
    }

    /**
     * Returns true if the dictionary key exists in the dictionary, false if not.
     * @param DictionaryKey $key
     * @return bool
     */
    public function exists(DictionaryKey $key): bool
    {
        return isset($this->values[$key->toString()]);
    }

    /**
     * Clears the dictionary
     */
    private function clear(): void
    {
        $this->values = [];
        $this->currentIndex = 0;
    }

    /**
     * Returns the current value
     * @return mixed
     */
    #[ReturnTypeWillChange] public function current(): mixed
    {
        $currentKey = $this->keys[$this->currentIndex];
        return $this->get($currentKey);
    }

    /** Move to the next item */
    public function next(): void
    {
        $this->currentIndex++;
    }

    public function key(): DictionaryKey
    {
        return $this->keys[$this->currentIndex];
    }

    public function valid(): bool
    {
        return ($this->currentIndex < count($this->keys));
    }

    public function rewind(): void
    {
        $this->currentIndex = 0;
    }

    /**
     * Returns an array of all the keys in the dictionary
     */
    public function getKeys(): array
    {
        return $this->keys;
    }

    /**
     * Serializes the dictionary into an array.
     */
    public function toArray(): array
    {
        $this->rewind();

        $data = [];

        foreach ($this as $key => $value) {
            $data[(string)$key] = $value;
        }

        $this->rewind();

        return $data;
    }

    /**
     * Serialises the dictionary into JSON.
     */
    public function toJson(): string
    {
        return JsonHelper::arrayToJsonString($this->toArray());
    }

    /***
     * Returns the DictionaryKeyMode
     */
    public function getKeyMode(): DictionaryKeyMode
    {
        return $this->keyMode;
    }

    /**
     * Creates a new Dictionary key with the correct settings for the dictionary
     * @param string $keyName
     * @return DictionaryKey
     */
    public function createKey(string $keyName): DictionaryKey
    {
        return new DictionaryKey($keyName, $this);
    }
}
