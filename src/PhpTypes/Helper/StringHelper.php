<?php

namespace PhpTypes\Helper;

class StringHelper
{
    /**
    * Converts the passed string into camelCase
    * @param string $str
    * @param array $noStrip
    * @return mixed|string|string[]|null
    */
    public static function camelCase(string $str, array $noStrip = [])
    {
        // non-alpha and non-numeric characters become spaces
        $str = preg_replace('/[^a-z0-9' . implode("", $noStrip) . ']+/i', ' ', $str);
        $str = trim($str);
        // uppercase the first character of each word
        $str = ucwords($str);
        $str = str_replace(" ", "", $str);
        $str = lcfirst($str);

        return $str;
    }
}
