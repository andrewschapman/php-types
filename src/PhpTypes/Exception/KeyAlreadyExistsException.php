<?php

namespace PhpTypes\Exception;

class KeyAlreadyExistsException extends \DomainException
{
    public function __construct(string $keyName)
    {
        parent::__construct("The key {$keyName} already exists and may not be overwritten");
    }
}
